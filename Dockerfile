FROM node:10.15.3-alpine

ENV NODE_ENV production
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin

WORKDIR /usr/src/app

COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]

RUN npm install --production --silent
# && mv node_modules ../ (caused issues with next.js)

COPY . .

EXPOSE 3000

RUN npm run build
CMD npm run start

USER node