/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';

function Header() {
  return (
    <header className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
      <h5 className="my-0 mr-md-auto font-weight-normal">WelpNathan</h5>
      <nav className="my-2 my-md-0 mr-md-3">
        <a className="p-2 text-dark" href="#">Development</a>
        <a className="p-2 text-dark" href="#">Security</a>
        <a className="p-2 text-dark" href="#">Projects</a>
        <a className="p-2 text-dark" href="#">About</a>
      </nav>
      <a className="btn btn-outline-primary" href="#">Sign up</a>
      <a className="btn btn-link" href="#">Log in</a>
    </header>
  );
}

export default Header;
