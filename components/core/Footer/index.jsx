/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';

function Footer() {
  return (
    <footer className="container pt-4 my-md-5 pt-md-5 border-top">
      <div className="row">
        <div className="col-12 col-md">
          <p className="d-block mb-3 text-muted">welpnathan.com</p>
          <small className="d-block mb-3 text-muted">&copy; 2019</small>
        </div>
        <div className="col-6 col-md">
          <h5>Something</h5>
          <ul className="list-unstyled text-small">
            <li><a className="text-muted" href="#">Link Link Link</a></li>
            <li><a className="text-muted" href="#">Link Link Link</a></li>
            <li><a className="text-muted" href="#">Link Link Link</a></li>
            <li><a className="text-muted" href="#">Link Link Link</a></li>
          </ul>
        </div>
        <div className="col-6 col-md">
          <h5>Something</h5>
          <ul className="list-unstyled text-small">
            <li><a className="text-muted" href="#">Link Link Link</a></li>
            <li><a className="text-muted" href="#">Link Link Link</a></li>
            <li><a className="text-muted" href="#">Link Link Link</a></li>
            <li><a className="text-muted" href="#">Link Link Link</a></li>
          </ul>
        </div>
        <div className="col-6 col-md">
          <h5>Something</h5>
          <ul className="list-unstyled text-small">
            <li><a className="text-muted" href="#">Link Link Link</a></li>
            <li><a className="text-muted" href="#">Link Link Link</a></li>
            <li><a className="text-muted" href="#">Link Link Link</a></li>
            <li><a className="text-muted" href="#">Link Link Link</a></li>
          </ul>
        </div>
      </div>
      <a href="https://seal.beyondsecurity.com/vulnerability-scanner-verification/welpnathan.com" />
      <img src="https://seal.beyondsecurity.com/verification-images/welpnathan.com/vulnerability-scanner-2.gif" alt="Website Security Test" border="0" />
    </footer>
  );
}

export default Footer;
