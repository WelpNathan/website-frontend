import React from 'react';
import App, { Container } from 'next/app';
import Head from 'next/head';
import Router from 'next/router';

import { pageview } from '../lib/gtag';

import '../static/css/main.css';

export default class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <Container>
        <Head>
          <title>Backend Software Engineer | welpnathan.com</title>
        </Head>
        <Component {...pageProps} />
      </Container>
    );
  }
}

Router.events.on('routeChangeComplete', url => pageview(url));
