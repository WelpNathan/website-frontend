const withCss = require('@zeit/next-css');

module.exports = withCss({
  publicRuntimeConfig: {
    GA_TRACKING_ID: 'UA-139528850-2',
  },
  webpackDevMiddleware: (config) => {
    // eslint-disable-next-line no-param-reassign
    config.watchOptions = {
      poll: 1000,
      aggregateTimeout: 300,
    };
    return config;
  },
});
